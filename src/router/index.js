import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/AppHome.vue'
import Listing from '../components/AppListing.vue'
import NewList from '../components/AppNewList.vue'
import ViewList from '../components/AppViewList.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/listing',
        name: 'listing',
        component: Listing
    },
    {
        path: '/new',
        name: 'new',
        component: NewList
    },
    {
        path: '/list/:listId',
        name: 'list',
        component: ViewList
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router

# JuListe2course

Pour utiliser ce projet, c'est simple :

```
git clone https://gitlab.com/engelbarte/juliste2course.git
```

```
cd juliste2course
```

```
npm install
```

```
npm run serve
```

Arbo de l'app :

- Home : rien de spécial
    - Listing : listing des listes et lien pour créer
    - Création : on crée une liste avec un nom
    - Vue : on voit la liste et on ajoute des éléments dedans

### Problème non résolu :

Dans AppResumeItem.vue quand je supprime un objet d'une liste il ne reload pas le component, j'ai ajouté un "hack" moche (`location.reload()`) pour émuler un reload.
